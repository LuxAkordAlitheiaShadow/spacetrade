package com.lux;

import com.lux.Planet.Planet;
import com.lux.Planet.PlanetFactory;
import com.lux.Products.Product;
import com.lux.Products.ProductFactory;
import com.lux.Ship.Ship;
import com.lux.Ship.ShipFactory;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 1/28/22
 * @Time: 3:51 PM
 */
public class CommercialSpace
{
    // Attributes.
    private int maxCycle;
    private CycleSingleton currentCycleSingleton;
    private ArrayList<Planet> planets;
    private ArrayList<Ship> ships;
    private ArrayList<Product> products;

    // Constructors.
    public CommercialSpace()
    {
        this.maxCycle = 100; // TODO make a rand()
        this.currentCycleSingleton = CycleSingleton.getInstance();

        // Creating products
        this.products = new ArrayList<Product>();
        ProductFactory productFactory = new ProductFactory();
        this.products = productFactory.createProductList();

        // Creating planet with products
        this.planets = new ArrayList<Planet>();
        PlanetFactory planetFactory = new PlanetFactory();
        this.planets = planetFactory.createPlanetList(products);

        // Creating ships
        this.ships = new ArrayList<Ship>();
        ShipFactory shipFactory = new ShipFactory();
        this.ships = shipFactory.createShipsList(planets);
    }

    // Getters.

    public CycleSingleton getCurrentCycleSingleton()
    {
        return this.currentCycleSingleton;
    }

    public int getMaxCycle()
    {
        return this.maxCycle;
    }

    // Methods.

    // Method for listing the products in this commercial space
    private void listProducts()
    {
        int indexProduct;
        System.out.println("List of the product in this commercial space :");
        // All products loop
        for (indexProduct = 0; indexProduct < products.size(); indexProduct++)
        {
            products.get(indexProduct).printAttributes();
            System.out.println("");
        }
    }

    // Method for listing the ships in this commercial space
    private void listShips()
    {
        int indexShip;
        System.out.println("List of the ships in this commercial space :");
        // All products loop
        for (indexShip = 0; indexShip < ships.size(); indexShip++)
        {
            ships.get(indexShip).printAttributes();
            System.out.println("");
        }
    }

    // Method for listing the planets in this commercial space
    private void listPlanets()
    {
        int indexPlanet;
        System.out.println("List of the planets in this commercial space :");
        // All products loop
        for (indexPlanet = 0; indexPlanet < planets.size(); indexPlanet++)
        {
            planets.get(indexPlanet).printAttributes();
            System.out.println("");
        }
    }

    // Method executing a cycle
    public void doCycle() throws SpaceException
    {
        // Increment current cycle
        this.currentCycleSingleton.setCurrentCycle(this.currentCycleSingleton.getCurrentCycle() + 1);
        // Randing value for know if we add a new Ship to the commercial space
        Random randomizer = new Random();
        int randomValue = randomizer.nextInt(5);
        // Add new ship in commercial space statement
        if ( randomValue == 1 )
        {
            ShipFactory shipFactory = new ShipFactory();
            this.ships.add( shipFactory.createShip(ships.size() + 1, this.planets) );
            System.out.println("A new ship was added.");
        }

        // Do cycle for each ships
        int indexShip;
        for (indexShip = 0; indexShip < ships.size(); indexShip++)
        {
            if ( ships.get(indexShip).getAlive() )
            {
                ships.get(indexShip).doCycleShip();
            }
        }
    }

    // Method asking user what he wants to see
    public void viewThings()
    {
        System.out.println("What do you want to see? [1/2]" +
                "\n 1- List" +
                "\n 2- Summarise");

        Scanner whatToSeeResponse = new Scanner(System.in);
        // The input is an integer statement
        if ( whatToSeeResponse.hasNextInt() )
        {
            int valueWhatToSeeResponse = whatToSeeResponse.nextInt();
            // User want to view a summarise statement
            if (valueWhatToSeeResponse == 1)
            {
                this.viewList();
            }
            // User want to view a list statement
            else
            {
                this.viewSummarise();
            }
        }
        // The input isn't an integer statement
        else
        {
            System.out.println("Input error ! Please enter an integer the next time is asked.");
        }
    }

    // Method asking user what type for list he wants to see
    private void viewList()
    {
        // Asking what user want to see
        System.out.println("What type of object do you want to see ? [1/2/3]" +
                "\n 1- Planet" +
                "\n 2- Ship" +
                "\n 3- Product");
        Scanner typeResponse = new Scanner(System.in);
        // The input is an integer statement
        if ( typeResponse.hasNextInt() )
        {
            int valueTypeResponse = typeResponse.nextInt();
            // User want to view the list of Planet statement
            if (valueTypeResponse == 1)
            {
                this.listPlanets();
            }
            // User want to view the list Ship statement
            else if (valueTypeResponse == 2)
            {
                this.listShips();
            }
            // User want to view the list of product
            else
            {
                // Viewing products
                this.listProducts();
            }
        }
        // The input isn't an integer statement
        else
        {
            System.out.println("Input error ! Please enter an integer the next time is asked.");
        }
    }

    // Method asking user what type for summarise he wants to see
    private void viewSummarise()
    {
        // Asking what user want to see
        System.out.println("What type of object do you want to see ? [1/2]" +
                "\n 1- Planet" +
                "\n 2- Ship");
        Scanner typeResponse = new Scanner(System.in);
        // The input is an integer statement
        if ( typeResponse.hasNextInt() )
        {
            int valueTypeResponse = typeResponse.nextInt();
            // User want to view a summarise of Planet statement
            if (valueTypeResponse == 1)
            {
                this.viewPlanetSumarise();
            }
            // User want to view a summarise of Ship statement
            else
            {
                this.viewShipSumarise();
            }
        }
        // The input isn't an integer statement
        else
        {
            System.out.println("Input error ! Please enter an integer the next time is asked.");
        }
    }

    // Method asking which planet user want to see the sumarise
    private void viewPlanetSumarise()
    {
        // Printing the list of planets
        int indexPlanet;
        System.out.println("Planets in the space trade :");
        for (indexPlanet = 0; indexPlanet < planets.size(); indexPlanet++)
        {
            System.out.println((indexPlanet + 1) + "- " + planets.get(indexPlanet).getName());
        }
        // Asking which planet the user want to see this sumarise
        System.out.println("Which planet do you want to see the sumarise [1-" + indexPlanet + "]");
        Scanner planetResponse = new Scanner(System.in);
        // The input is an integer statement
        if ( planetResponse.hasNextInt() )
        {
            int valuePlanetResponse = planetResponse.nextInt();
            // Calling the planet's sumarise
            planets.get(valuePlanetResponse - 1).summarise();
        }
        // The input isn't an integer statement
        else
        {
            System.out.println("Input error ! Please enter an integer the next time is asked.");
        }
    }

    // Method asking which ship user want to see the sumarise
    private void viewShipSumarise()
    {
        // Printing the list of ships
        int indexShip;
        System.out.println("Planets in the space trade :");
        for (indexShip = 0; indexShip < ships.size(); indexShip++)
        {
            System.out.println((indexShip + 1) + "- " + ships.get(indexShip).getMatricule());
        }
        // Asking which planet the user want to see this sumarise
        System.out.println("Which ship do you want to see the sumarise [1-" + indexShip + "]");
        Scanner shipResponse = new Scanner(System.in);
        // The input is an integer statement
        if ( shipResponse.hasNextInt() )
        {
            int valueShipResponse = shipResponse.nextInt();
            // Calling the ship's sumarise
            ships.get(valueShipResponse - 1).summarise();
        }
        // The input isn't an integer statement
        else
        {
            System.out.println("Input error ! Please enter an integer the next time is asked.");
        }
    }
}