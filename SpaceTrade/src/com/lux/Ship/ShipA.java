package com.lux.Ship;

import com.lux.CycleSingleton;
import com.lux.Planet.Planet;
import com.lux.Products.Dangerous.Uranium;
import com.lux.Products.Product;
import com.lux.Products.Safe.Banana;
import com.lux.Products.Safe.Wood;
import com.lux.Transactions.Transaction;
import com.lux.Products.ConstantString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.lux.Products.ConstantString.*;

/**
 * @Package: com.thomas
 * @Author: Thomas GOUYET
 * @Date: 1/28/22
 * @Time: 4 : 10 PM
 */
/**
 * @Package: com.thomas
 * @Author: Thomas GOUYET
 * @Date: 1/28/22
 * @Time: 4 : 10 PM
 */
public final class ShipA extends Ship
{
    private String className;

    //Constructor
    public ShipA(int id, ArrayList<Planet> planets)
    {
        className = "A";
        matricule = "ShipA_" + id;
        this.currentCycleSingleton = CycleSingleton.getInstance();
        lifeTime = 15; // The number of cycle before the destruction of the ship
        shipState=2; //The ship is trading
        saleState = true; // The ship is buying
        indiceItinerary = 0;
        transactions = new ArrayList<Transaction>();
        itinerary = new ArrayList<Planet>();
        createItinerary(planets);

        inventory = new HashMap<Product, Integer>();
        inventory.put(new Uranium(),0);
        inventory.put(new Banana(),0);
        inventory.put(new Wood(),0);

        capacity = new HashMap<String, Integer>();
        capacity.put(URANIUM, 25);
        capacity.put(BANANA, 50);
        capacity.put(WOOD, 60);

        alive = true;

        // Affecting ships to planets
        int var;
        var = itinerary.get(indiceItinerary).canEmbark(shipState);
        switch(var)
        {
            // The ship can go to a port and trade
            case 0:
                shipState=2;
                break;
            //There is no port available, so the ship is waiting in orbit
            case 1:
                shipState=3;
                break;
            // There is no orbite available, the ship is destroy
            case 2:
                destroy();
                break;
        }
    }

    public void printAttributes()
    {
        System.out.println("Matricule : "+this.matricule);
        System.out.println("Class : "+this.className);
        switch(shipState)
        {
            case 0 :
                System.out.println("Ship state : "+this.shipState+" -> The ship is travels");
                break;
            case 1:
                System.out.println("Ship state : "+this.shipState+" -> The ship want to embark");
                break;
            case 2:
                System.out.println("Ship state : "+this.shipState+" -> The ship trades");
                break;
            case 3:
                System.out.println("Ship state : "+this.shipState+" -> The ship waits in a orbite");
                break;
        }
        System.out.println("LifeTime : "+this.lifeTime);
        if(saleState)
        {
            System.out.println("SaleState : "+this.saleState+" -> Buy");
        }else
        {
            System.out.println("SaleState : "+this.saleState+" -> Sell");
        }

        System.out.println("indiceItinerary : "+this.indiceItinerary);
        // Printing itinerary
        int indexItinerary;
        System.out.println("itinerary :");
        for( indexItinerary = 0 ; indexItinerary < itinerary.size() ; indexItinerary++ )
        {
            System.out.println("\t"+itinerary.get(indexItinerary).getName());
        }
        System.out.println("Inventory : ");
        for(Product product : inventory.keySet())
        {
            System.out.println("\tProduct "+product.getLibelle()+" | value : "+inventory.get(product)+" T");
        }
        System.out.println("Capacity : ");
        for(Map.Entry product : capacity.entrySet())
        {
            System.out.println("\tProduct "+product.getKey()+" | Max : "+product.getValue()+" T");
        }
    }
}


