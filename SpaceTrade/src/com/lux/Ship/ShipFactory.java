package com.lux.Ship;

import com.lux.Planet.Planet;
import com.lux.Products.Product;
import com.lux.SpaceException;

import java.util.ArrayList;
import java.util.Random;

/**
 * @Package: com.thomas
 * @Author: Thomas GOUYET
 * @Date: 02/07/22
 * @Time: 4 : 10 PM
 */
public class ShipFactory
{
    /*
    Return a array of 3 ships with the 3 class
     */
    public ArrayList<Ship>createShipsList(ArrayList<Planet> planets)
    {
        // Initializing products array
        ArrayList<Ship> arrayShips = new ArrayList<Ship>();

        // Creating products
        arrayShips.add(new ShipA(1, planets));
        arrayShips.add(new ShipB(2, planets));
        arrayShips.add(new ShipC(3, planets));

        // Returning list of Product
        return arrayShips;
    }

    /*
     Return a ship with a random class
     */
    public Ship createShip(int id, ArrayList<Planet> planets) throws SpaceException
    {
        Random rand = new Random();
        Ship newShip;
        int int_random = rand.nextInt(1, 3);
        switch(int_random)
        {
            case 1:
                newShip = new ShipA(id, planets);
                break;
            case 2:
                newShip = new ShipB(id, planets);
                break;
            case 3:
                newShip = new ShipC(id, planets);
                break;
            default:
                throw new SpaceException("Erreur in ShipFactory in createShip : Bad rand int");
        }
        return newShip;
    }
}
