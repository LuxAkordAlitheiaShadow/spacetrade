package com.lux.Ship;

import com.lux.CycleSingleton;
import com.lux.Planet.Planet;
import com.lux.Products.Dangerous.Deuterium;
import com.lux.Products.Product;
import com.lux.Products.Safe.Water;
import com.lux.Products.Safe.Wood;
import com.lux.Transactions.Transaction;
import com.lux.Products.ConstantString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Package: com.thomas
 * @Author: Thomas GOUYET
 * @Date: 1/28/22
 * @Time: 17: 10 PM
 */
/**
 * @Package: com.thomas
 * @Author: Thomas GOUYET
 * @Date: 1/28/22
 * @Time: 17: 10 PM
 */
public final class ShipB extends Ship
{
    private String className;

    //Constructor
    public ShipB(int id, ArrayList<Planet> planets)
    {
        int var;
        className = "B";
        matricule="ShipB_"+id;
        this.currentCycleSingleton = CycleSingleton.getInstance();
        lifeTime = 15; // The number of cycle before the destruction of the ship
        shipState=2; //The ship is trading
        saleState = true; // The ship is buying
        indiceItinerary = 0;
        transactions = new ArrayList<Transaction>();
        itinerary = new ArrayList<Planet>();
        createItinerary(planets);

        inventory = new HashMap<Product, Integer>();
        inventory.put(new Water(), 0);
        inventory.put(new Deuterium(), 0);
        inventory.put(new Wood(), 0);

        alive = true;

        capacity = new HashMap<String, Integer>();
        capacity.put(ConstantString.WATER, 100);
        capacity.put(ConstantString.DEUTERIUM, 55);
        capacity.put(ConstantString.WOOD, 60);

        // Affecting ships to planets
        var = itinerary.get(indiceItinerary).canEmbark(shipState);
        switch(var)
        {
            // The ship can go to a port and trade
            case 0:
                shipState=2;
                break;
            //There is no port available, so the ship is waiting in orbit
            case 1:
                shipState=3;
                break;
            // There is no orbite available, the ship is destroy
            case 2:
                destroy();
                break;
        }
    }

    public void printAttributes()
    {
        System.out.println("Matricule : "+this.matricule);
        System.out.println("Class : "+this.className);
        switch(shipState)
        {
            case 0 :
                System.out.println("Ship state : "+this.shipState+" -> The ship travels");
                break;
            case 1:
                System.out.println("Ship state : "+this.shipState+" -> The ship want to embark");
                break;
            case 2:
                System.out.println("Ship state : "+this.shipState+" \n" + "-> The ship trades");
                break;
            case 3:
                System.out.println("Ship state : "+this.shipState+" -> The ship waits in a orbite");
                break;
        }
        System.out.println("LifeTime : "+this.lifeTime);
        if(saleState)
        {
            System.out.println("SaleState : "+this.saleState+" -> Buy");
        }else
        {
            System.out.println("SaleState : "+this.saleState+" -> Sell");
        }

        System.out.println("indiceItinerary : "+this.indiceItinerary);
        // Printing itinerary
        int indexItinerary;
        System.out.println("itinerary :");
        for( indexItinerary = 0 ; indexItinerary < itinerary.size() ; indexItinerary++ )
        {
            System.out.println("\t"+itinerary.get(indexItinerary).getName());
        }
        System.out.println("Inventory : ");
        for(Product product : inventory.keySet())
        {
            System.out.println("\tProduct "+product.getLibelle()+" | value : "+inventory.get(product)+" T");
        }
        System.out.println("Capacity : ");
        for(Map.Entry product : capacity.entrySet())
        {
            System.out.println("\tProduct "+product.getKey()+" | Max : "+product.getValue()+" T");
        }
    }
}
