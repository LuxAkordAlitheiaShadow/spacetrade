package com.lux.Ship;

import com.lux.CycleSingleton;
import com.lux.Planet.Planet;
import com.lux.Transactions.Transaction;
import  com.lux.Products.Product;

import java.util.*;

/**
 * @Package: com.thomas
 * @Author: Thomas GOUYET
 * @Date: 1/28/22
 * @Time: 4 : 10 PM
 */
public abstract class Ship
{
    // Attributes.
    protected String matricule; // ID 's ship
    protected int lifeTime; // Number of cycles remaining; At 0, the ship is destroy
    protected int shipState; // Ship'state : 0 - the ship is in transit to a another planet ; 1 - the ship want to embark in a planet  ; 2- the ship is buying/selling ; 3 - the ship waits in the orbit of a port;
    protected boolean saleState; // True if the ship is buying, false if the ship is selling
    protected ArrayList <Transaction> transactions; //List of the ship's transaction (cycle's number, planet and products)
    protected ArrayList<Planet> itinerary; // Ship's itinerary (list of the planets) // Ship's itinerary (list of the planets)
    protected int indiceItinerary; // The actual planet of the ship
    protected HashMap<Product,Integer> inventory; // Ship's inventory
    protected HashMap<String,Integer> capacity; // Quantity's limit of the products in the ship
    protected CycleSingleton currentCycleSingleton;
    protected boolean alive;

    // Getters.
    public String getMatricule()
    {
        return this.matricule;
    }

    public boolean getAlive()
    {
        return this.alive;
    }

    // Setters.
    public void setItinerary(ArrayList<Planet> newItinerary)
    {
        this.itinerary = newItinerary;
    }

    private void setAlive(boolean aliveValue)
    {
        this.alive = aliveValue;
    }

    // Methods.

    // This function print all about the ship's trade
    public void summarise()
    {
        System.out.println("- "+matricule);
        for(Transaction val : transactions)
        {
            System.out.println("\tCycle : "+val.getCycle()+" , Target : "+val.getTarget()+" , SaleType = "+val.getSaleType());
            for(Map.Entry product : val.getProductsSold().entrySet())
            {
                System.out.println("\t\t Product "+product.getKey()+" | value : "+product.getValue());
            }
        }
    }

    /*
    Main function of the ship
    We check the ship state and the ship acts accordingly
     */
    public void doCycleShip()
    {
        // We decrement the life time of this ship
        lifeTime--;
        //We check if the ship is still alive
        if(lifeTime>0)
        {
            // We check the ship's state
            switch(shipState)
            {
                //The ship is in transit
                case 0:
                    shipState = 1; //In the next cycle, the ship can embark in a planet
                    break; // We stop this ship for this cycle
                //The ship ask to a planet if it can embark
                case 1: case 3:
                    embark();
                    break;
                //Commerce
                case 2:
                    trade();
                    break;
            }
        }else
        {
            // This ship has reached the end of its service life and is destroy
            destroy();
        }
    }

    /*

     */
    public void trade()
    {
        // We check the sale state of this ship - True, the ship is buying | False, the ship is selling
        if(saleState)
        {
            isBuying();
            // We check if we can continue to buy on the current planet
            if(cantBuy())
            {
                // If this ship cannot buy on this planet, it travels to the next planet.
                travel();
            }
        }else
        {
            isSelling();
            // We check if we can continue to sell on the current planet, it travels to the next planet
            if(cantSell())
            {
                // If this ship cannot sell on this planet, it travels to the next planet.
                travel();
            }
        }
    }

    /*
    This function check if a port is available on the current planet
     */
    public void embark()
    {
        int var=0;
        /*
        We check on the current planet if there is an available port
        (0 - a port is available; 1 - no port is available but the ship can go in a orbite; 2 - Nothing is available in this planet, the ship is destroy)
        */
        var = itinerary.get(indiceItinerary).canEmbark(shipState);
        switch(var)
        {
            // The ship can go to a port and trade
            case 0:
                shipState=2;
                trade();
                break;
            //There is no port available, so the ship is waiting in orbit
            case 1:
                shipState=3;
                break;
            // There is no orbite available, the ship is destroy
            case 2:
                destroy();
                break;
        }
    }

    /*
    This function try to sell a maximum of products in a cycle
     */
    public void isSelling()
    {
        // this variable chek if a product is the first product that the ship can sell in inventory
        boolean isFirstProduct = true;
        // We get the variable dangerous to the first product that the ship can sell in inventory
        // With that, we can't sell another type of dangerous product than the first product we sell in a cycle
        boolean isDangerous=true;
        // This variable is used for sell a maximum in one cycle
        int nbProduct=0;
        for(Product product : inventory.keySet())
        {
            // We check if we can sell this product
            if(inventory.get(product)>0 && itinerary.get(indiceItinerary).canBuy(product,1)>0)
            {
                // We check if this product is the first product we sell in this cycle
                if(isFirstProduct)
                {
                    isFirstProduct=false;
                    // We get the variable dangerous th this product
                    isDangerous=product.getDangerous();
                    // We set a value to nbValue in function to the dangerousness product
                    if(isDangerous)
                    {
                        nbProduct=10;
                    }else
                    {
                        nbProduct=25;
                    }
                }
                // We check if the dangerousness of this product is the same than the first product
                // and we remove to nbValue the quantity that has been sold
                if(product.getDangerous()==isDangerous)nbProduct=-sell(product,nbProduct);
                // If nbProduct = 0, this mean this ship can t sell anymore and we leave this function
                if(nbProduct==0)
                {
                    return;
                }
            }
        }
        return;
    }

    /*
    This function try to buy a maximum of products in a cycle
     */
    public void isBuying()
    {
        // this variable chek if a product is the first product that the ship can sell in inventory
        boolean isFirstProduct = true;
        // We get the variable dangerous to the first product that the ship can sell in inventory
        // With that, we can't sell another type of dangerous product than the first product we sell in a cycle
        boolean isDangerous=true;
        // This variable is used for sell a maximum in one cycle
        int nbProduct=0;
        for(Product product : inventory.keySet())
        {
            // We check if we can buy this product
            if(inventory.get(product)< capacity.get(product.getLibelle()) && itinerary.get(indiceItinerary).haveProduct((product)))
            {
                // We check if this product is the first product we sell in this cycle
                if(isFirstProduct)
                {
                    isFirstProduct = false;
                    // We get the variable dangerous th this product
                    isDangerous = product.getDangerous();
                    // We set a value to nbValue in function to the dangerousness product
                    if (isDangerous)
                    {
                        nbProduct = 20;
                    } else
                    {
                        nbProduct = 50;
                    }
                }
                // We check if the dangerousness of this product is the same than the first product
                // and we remove to nbValue the quantity that has been purchased
                if(product.getDangerous()==isDangerous)nbProduct=-buy(product,nbProduct);
                // If nbProduct = 0, this mean this ship can t buy anymore and we leave this function
                if(nbProduct<=0)return;
            }
        }
        return;
    }

    /*
    Add a quantity of a product to inventory.
    prod : the products to load.
    quantity : the quantity to add to the products.
    return the value of this product that was purchased
     */
    public int buy(Product prod,int quantity)
    {
        // The real value that was purchased
        // It is used in the case where the current planet is not empty for this product but can t sell all of the quantity value
        int buyingProduct = 0;
        //we get the actual value of this product in the inventory
        int val = inventory.get(prod);
        // We check if the ship can load this quantity of the product
        if(val+quantity<capacity.get(prod.getLibelle()))
        {
            // This ship try to buy the var quantity to the current planet
            buyingProduct = itinerary.get(indiceItinerary).sell(prod,quantity,matricule);
        }else
        {
            buyingProduct=itinerary.get(indiceItinerary).sell(prod,capacity.get(prod.getLibelle())-val,matricule);
        }
        // We add the number buyingProduct to the inventory for this product
        inventory.put(prod,val+buyingProduct);
        // We add this transaction to the var transactions
        transactions.add(new Transaction(itinerary.get(indiceItinerary).getName(),currentCycleSingleton.getCurrentCycle(),prod.getLibelle(),buyingProduct,"Buy"));
        return  buyingProduct;
    }

    /*
    Remove a quantity of a product to inventory.
    prod : the products to unload.
    quantity : the quantity to remove to the products.
    return the value of this product that was sold.
     */
    public int sell(Product prod, int quantity)
    {
        // The real value that was sold
        // It is used in the case where the current planet is not full for this product but can t buy all of the quantity value
        int sellingProduct = 0;
        //we get the actual value of this product in the inventory
        int val = inventory.get(prod);
        // We check if the ship can unload this quantity of this product
        if(val>=quantity)
        {
            // This ship try to sell the var quantity to the current planet
            sellingProduct=itinerary.get(indiceItinerary).buy(prod,quantity,matricule);
        }else
        {
            sellingProduct=itinerary.get(indiceItinerary).buy(prod,val,matricule);;
        }
        // We remove the number sellingProduct to the inventory for this product
        inventory.put(prod,val-sellingProduct);
        // We add this transaction to the var transactions
        transactions.add(new Transaction(itinerary.get(indiceItinerary).getName(),currentCycleSingleton.getCurrentCycle(),prod.getLibelle(),sellingProduct,"Sell"));
        return sellingProduct;
    }

    /*
    The ship travel to the next planet in this itinerary
    If the ship has already see all the planet on this itinerary, the ship restart this itinerary
    */
    protected void travel()
    {
        // We inform the current planet that this ship has left this port
        itinerary.get(indiceItinerary).debark();
        // We set the ship state to the travel state
        shipState=0;
        // We invert the saleState
        saleState=!saleState;
        // We increase the var indiceItinerary
        indiceItinerary++;
        // If the ship has already seen all the planets on this itinerary, it starts this itinerary again
        if(indiceItinerary==(itinerary.size())) indiceItinerary=0;
    }

    /*
    This function check if this ship can sell a product in this inventory on the current planet
     */
    protected boolean cantSell()
    {
        int var = 0;
        for (Product product : inventory.keySet())
        {
            // We check in the inventory if the ship has a product who can be sell
            if(inventory.get(product)>0 && itinerary.get(indiceItinerary).canBuy(product,1)>0)var++;
        }
        if(var>0)
        {
            // This ship don t need to leave the current planet
            return false;
        }else
        {
            // This ship has no products it can sell
            // This ship need to leave the current planet
            return true;
        }
    }

    /*
    This function check if this ship can buy a product in this inventory on the current planet
     */
    protected boolean cantBuy()
    {
        int var = 0;
        for (Product product : inventory.keySet())
        {
            // We check in the inventory if the ship has a product who can be buy
            if(inventory.get(product)==capacity.get(product.getLibelle()) || !itinerary.get(indiceItinerary).haveProduct((Product) product))
            {
                var++;
            }
        }
        if(var==inventory.size())
        {
            // This ship has no products it can buy
            // This ship need to leave the current planet
            return true;
        }else
        {
            // This ship don t need to leave the current planet
            return false;
        }
    }

    /*
    This function set the var isAlive to false
     */
    public void destroy()
    {
        System.out.println("The ship " + this.matricule + " has been destroyed.");
        this.setAlive(false);
    }

    /*
    This function print all information about this ship to the use
     */
    public void printAttributes(){}

    // Method for affecting an itinerary to a ship
    protected void createItinerary(ArrayList<Planet> planets)
    {
        // Generating list of possibilities
        ArrayList<Integer> possibilitiesList = new ArrayList<Integer>();
        int indexPlanet;
        for ( indexPlanet = 0 ; indexPlanet < planets.size() ; indexPlanet++ )
        {
            possibilitiesList.add(indexPlanet);
        }
        // Generating itinerary
        ArrayList<Planet> newItinerary = new ArrayList<Planet>();
        Random randomizer = new Random();
        int numberOfDestination = randomizer.nextInt(2, planets.size());
        for ( indexPlanet = 0 ; indexPlanet < numberOfDestination ; indexPlanet++ )
        {
            int destination = randomizer.nextInt(possibilitiesList.size());
            newItinerary.add(planets.get(possibilitiesList.get(destination)));
            possibilitiesList.remove(destination);
        }
        // Affecting itinerary
        this.setItinerary(newItinerary);
    }

}
