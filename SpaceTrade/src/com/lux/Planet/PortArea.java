package com.lux.Planet;

import com.lux.Products.Product;

import java.util.HashMap;

public class PortArea
{
    private int size;
    private int embarked = 0;
    private int awaiting = 0;

    public PortArea(int size)
    {
        this.size = size;
        //Init port inventory
    }

    public void debark()
    {
        //If the ship's inventory is full OR can't load the products, it leaves the port
        embarked--;
    }

    public int canEmbark(int state)
    {
        if(embarked < size) //There is enough space for the ship
        {
            if(state == 3) //The ship was waiting, it frees up space in orbit and take one in the port
            {
                awaiting--;
            }
            embarked++;
            return 0;
        }
        else if(embarked == size && (awaiting < size || state == 3)) //No port available, it has to wait in orbit
        {
            if(state == 3) //The ship is already waiting
            {
                return 1;
            }

            awaiting++;
            return 1;

        }
        else //Destroy the ship
        {
            return 2;
        }
    }
}
