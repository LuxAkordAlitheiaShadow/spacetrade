package com.lux.Planet;

import com.lux.Products.Product;
import java.util.ArrayList;

public class PlanetFactory
{
    private int quantityMin = 500;
    private int quantityMax = 1000;
    private int randomise;
    // Method returning list of Planet
    public ArrayList<Planet> createPlanetList(ArrayList<Product> products)
    {
        randomise = (int)Math.floor(Math.random() * (quantityMax - quantityMin + 1) + quantityMin);

        // Initializing planet array
        ArrayList<Planet> planet = new ArrayList<Planet>();
        // Creating planets
        planet.add(new Planet("Earth", products.get(0), randomise));
        planet.add(new Planet("Mars", products.get(1), randomise));

        planet.add(new Planet("Venus", products.get(2), randomise));

        planet.add(new Planet("Saturn", products.get(3), randomise));

        planet.add(new Planet("Jupiter", products.get(4), randomise));


        // Returning list of Planet
        return planet;
    }
}
