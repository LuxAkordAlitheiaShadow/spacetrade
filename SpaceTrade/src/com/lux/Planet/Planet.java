package com.lux.Planet;

import com.lux.CycleSingleton;
import com.lux.Products.Product;
import com.lux.Transactions.Transaction;
import com.lux.Products.ConstantString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.lux.Products.ConstantString.DEUTERIUM;

public class Planet
{
    private final String name;
    private final PortArea portArea;
    private final HashMap <String,Integer> inventory;
    private final HashMap<String,Integer> capacity;
    private final ArrayList<Transaction> transactions;
    protected CycleSingleton currentCycleSingleton;

    public Planet(String name, Product product, int quantity)
    {
        int portSizeMin = 1;
        int portSizeMax = 3;

        this.name = name;
        this.inventory = new HashMap<>();
        this.capacity = new HashMap<>();

        //Initializing all capacities
        this.capacity.put(ConstantString.DEUTERIUM, quantity);
        this.capacity.put(ConstantString.URANIUM, quantity);
        this.capacity.put(ConstantString.BANANA, quantity);
        this.capacity.put(ConstantString.WATER, quantity);
        this.capacity.put(ConstantString.WOOD, quantity);

        //Initializing all products
        this.inventory.put(ConstantString.DEUTERIUM, 0);
        this.inventory.put(ConstantString.URANIUM, 0);
        this.inventory.put(ConstantString.BANANA, 0);
        this.inventory.put(ConstantString.WATER, 0);
        this.inventory.put(ConstantString.WOOD, 0);
        //Give a quantity not null for one product
        this.inventory.put(product.getLibelle(), quantity);


        this.portArea = new PortArea((int)Math.floor(Math.random() * (portSizeMax - portSizeMin + 1) + portSizeMin));
        this.currentCycleSingleton = CycleSingleton.getInstance();
        this.transactions = new ArrayList<>();

    }

    public void printAttributes()
    {
        System.out.println("Planet name : " + name);
        for(Map.Entry var : inventory.entrySet())
        {
            System.out.println("Product : " + var.getKey() +
                                System.getProperty("line.separator") +
                               "Quantity : " + var.getValue() + "T");
        }
    }

    public int canEmbark(int state)
    {
        return portArea.canEmbark(state);
    }

    public void debark()
    {
        portArea.debark();
    }

    public int buy(Product product, int quantity, String shipName)
    {
        //We check, adjust and add the quantity bought depending on capacity
        int quantityBought = canBuy(product, quantity);
        //The planet can't buy more products
        if(quantityBought == 0)
        {
            return quantityBought;
        }
        //The planet buy all the product it can take
        else
        {
            inventory.put(product.getLibelle(), inventory.get(product.getLibelle()) + quantityBought);

            //Add the transaction to the history
            transactions.add(new Transaction(shipName, currentCycleSingleton.getCurrentCycle(), product.getLibelle(), quantityBought, "Buy"));

            return quantityBought;
        }

    }

    public int sell(Product product, int quantity, String shipName)
    {
        //The planet has enough products for the ship
        if(inventory.get(product.getLibelle()) >= quantity)
        {
            inventory.put(product.getLibelle(), inventory.get(product.getLibelle()) - quantity);
            transactions.add(new Transaction(shipName, currentCycleSingleton.getCurrentCycle(), product.getLibelle(), quantity, "Sell"));
            return quantity;
        }
        //The planet gives all his remaining products
        else
        {
            quantity = inventory.get(product.getLibelle());
            inventory.put(product.getLibelle(), 0);
            transactions.add(new Transaction(shipName, currentCycleSingleton.getCurrentCycle(), product.getLibelle(), quantity, "Buy"));
            return quantity;
        }
    }

    public boolean haveProduct(Product product)
    {
        for(Map.Entry<String,Integer> var : inventory.entrySet())
        {
            if(var.getKey() == product.getLibelle() && var.getValue() != 0){
                return true;
            }
        }
        return false;
    }

    public void summarise()
    {
        System.out.println("- "+ name);
        for (Transaction val : transactions) {
            System.out.println("\tCycle : " + val.getCycle() + " , Target : " + val.getTarget() + " , SaleType = " + val.getSaleType());
            for (Map.Entry product : val.getProductsSold().entrySet()) {
                System.out.println("\t\t Product " + product.getKey() + " | value : " + product.getValue() + "T");
            }
        }
    }

    public String getName()
    {
        return name;
    }

    public void showInventory()
    {
        for(String s : inventory.keySet())
        {
            System.out.println("Product : "+s+" | Value : "+inventory.get(s));
        }
    }

    public int canBuy(Product product, int quantity)
    {
        int spaceRemaining = capacity.get(product.getLibelle()) - inventory.get(product.getLibelle());
        //The planet have enough capacity to buy all the products
        if(spaceRemaining >= quantity)
        {
            return quantity;
        }
        //Return the amount of product the planet can buy
        else
        {
            return spaceRemaining;
        }
    }

}
