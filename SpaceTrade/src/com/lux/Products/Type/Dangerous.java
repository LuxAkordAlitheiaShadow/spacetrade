package com.lux.Products.Type;

import com.lux.Products.Product;

/**
 * @Package: com.lux.Products.Type
 * @Author: Titouan 'Lux' Allain
 * @Date: 1/31/22
 * @Time: 6:31 PM
 */
public abstract class Dangerous extends Product
{
    // Attributes.
    protected String libelle;
    protected boolean dangerous;

    // Constructors.
    public Dangerous()
    {
        this.dangerous = true;
    }

    // Getters.
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    // Methods.

    // Parent method for printing child attributes
    public void printAttributes()
    {
        System.out.println("Not supposed to call this methods from Dangerous");
    }
}
