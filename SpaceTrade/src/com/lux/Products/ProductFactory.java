package com.lux.Products;

import com.lux.Products.Dangerous.Deuterium;
import com.lux.Products.Dangerous.Uranium;
import com.lux.Products.Safe.Banana;
import com.lux.Products.Safe.Water;
import com.lux.Products.Safe.Wood;

import java.util.ArrayList;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 1/31/22
 * @Time: 6:11 PM
 */
public class ProductFactory
{
    // Methods.

    // Method returning list of Product
    public ArrayList<Product> createProductList()
    {
        // Initializing products array
        ArrayList<Product> products = new ArrayList<Product>();

        // Creating products
        products.add(new Wood());
        products.add(new Uranium());
        products.add(new Banana());
        products.add(new Deuterium());
        products.add(new Water());

        // Returning list of Product
        return products;
    }

}
