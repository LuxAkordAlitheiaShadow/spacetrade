package com.lux.Products;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 1/28/22
 * @Time: 3:55 PM
 */
public abstract class Product
{
    // Attributes.
    protected String libelle;
    protected boolean dangerous;

    // Constructors.
    public Product() {}

    // Getters.
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    public String getLibelle()
    {
        return this.libelle;
    }

    // Methods.

    // Parent method for printing child attributes.
    public void printAttributes()
    {
        System.out.println("Not supposed to call this methods from Product");
    }
}
