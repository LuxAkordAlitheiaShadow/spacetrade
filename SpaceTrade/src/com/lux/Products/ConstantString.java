package com.lux.Products;

/**
 * @Package: com.lux.Products
 * @Author: Titouan 'Lux' Allain
 * @Date: 1/31/22
 * @Time: 6:37 PM
 */
public final class ConstantString
{
    public static final String BANANA = "Banana";
    public static final String WOOD = "Wood";
    public static final String URANIUM = "Uranium";
    public static final String DEUTERIUM = "Deuterium";
    public static final String WATER = "Water";
}
