package com.lux.Products.Safe;

import com.lux.Products.Type.Safe;

import static com.lux.Products.ConstantString.WATER;

/**
 * @Package: com.lux.Products.Safe
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/10/22
 * @Time: 5:15 PM
 */
public final class Water extends Safe
{
    // Attributes.
    private String libelle;

    // Constructors.
    public Water()
    {
        libelle = WATER;
    }

    // Methods.
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    public String getLibelle()
    {
        return this.libelle;
    }
    // Printing attributes method
    public void printAttributes()
    {
        System.out.println(this.libelle);
        System.out.println(this.dangerous);
    }
}
