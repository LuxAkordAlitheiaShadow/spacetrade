package com.lux.Products.Safe;

import com.lux.Products.Type.Safe;

import static com.lux.Products.ConstantString.BANANA;

/**
 * @Package: com.lux.Products.Safe
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/2/22
 * @Time: 6:30 PM
 */
public final class Banana extends Safe
{
    // Attributes.
    private String libelle;
    private boolean dangerous;
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    public String getLibelle()
    {
        return this.libelle;
    }
    // Constructors.
    public Banana()
    {
        libelle = BANANA;
    }

    // Methods.

    // Printing attributes method
    public void printAttributes()
    {
        System.out.println(this.libelle);
        System.out.println(this.dangerous);
    }
}
