package com.lux.Products.Safe;

import com.lux.Products.Type.Safe;

import static com.lux.Products.ConstantString.WOOD;

/**
 * @Package: com.lux.Products
 * @Author: Titouan 'Lux' Allain
 * @Date: 1/31/22
 * @Time: 6:27 PM
 */
public final class Wood extends Safe
{
    // Attributes.
    private String libelle;

    // Constructors.
    public Wood()
    {
        libelle = WOOD;
    }

    // Methods.
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    public String getLibelle()
    {
        return this.libelle;
    }
    // Printing attributes method
    public void printAttributes()
    {
        System.out.println(this.libelle);
        System.out.println(this.dangerous);
    }
}
