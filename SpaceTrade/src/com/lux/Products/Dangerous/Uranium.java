package com.lux.Products.Dangerous;

import com.lux.Products.Type.Dangerous;

import static com.lux.Products.ConstantString.URANIUM;

/**
 * @Package: com.lux.Products.Dangerous
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/2/22
 * @Time: 6:21 PM
 */
public final class Uranium extends Dangerous
{
    // Attributes.
    private String libelle;
    private boolean dangerous;

    // Constructors.
    public Uranium()
    {
        libelle = URANIUM;
        dangerous = true; // A regler !!!!!
    }

    // Methods.
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    public String getLibelle()
    {
        return this.libelle;
    }
    // Printing attributes method
    public void printAttributes()
    {
        System.out.println(this.libelle);
        System.out.println(this.dangerous);
    }
}
