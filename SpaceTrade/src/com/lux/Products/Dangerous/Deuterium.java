package com.lux.Products.Dangerous;

import com.lux.Products.Type.Dangerous;

import static com.lux.Products.ConstantString.DEUTERIUM;

/**
 * @Package: com.lux.Products.Dangerous
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/10/22
 * @Time: 5:13 PM
 */
public final class Deuterium extends Dangerous
{
    // Attributes.
    private String libelle;
    private boolean dangerous;

    // Constructors.
    public Deuterium()
    {
        libelle = DEUTERIUM;
        dangerous=true;
    }

    // Methods.
    public boolean getDangerous()
    {
        return this.dangerous;
    }

    public String getLibelle()
    {
        return this.libelle;
    }
    // Printing attributes method
    public void printAttributes()
    {
        System.out.println(this.libelle);
        System.out.println(this.dangerous);
    }
}
