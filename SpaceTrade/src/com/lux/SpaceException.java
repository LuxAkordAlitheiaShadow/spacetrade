package com.lux;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/12/22
 * @Time: 4:16 PM
 */
public class SpaceException extends Exception
{
    // Constructors.
    public SpaceException(String message)
    {
        super(message);
    }
}
