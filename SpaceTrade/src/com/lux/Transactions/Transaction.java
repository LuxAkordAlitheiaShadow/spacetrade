package com.lux.Transactions;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * @Package: com.lux.Transactions
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/7/22
 * @Time: 4:16 PM
 */
public class Transaction
{
    // Attributes.

    private String target;
    private int cycle;
    private String saleType;
    private HashMap<String, Integer> productSold = new HashMap<String, Integer>();

    // Constructors.
    public Transaction(String target, int cycle, String productName, int productQuantity, String _saleType)
    {
        this.target = target;
        this.cycle = cycle;
        this.productSold.put(productName, productQuantity);
        this.saleType=_saleType;
    }


    public String getTarget()
    {
        return target;
    }

    public int getCycle()
    {
        return cycle;
    }

    public String getSaleType(){ return saleType;}

    public HashMap<String, Integer> getProductsSold()
    {
        return productSold;
    }

}
