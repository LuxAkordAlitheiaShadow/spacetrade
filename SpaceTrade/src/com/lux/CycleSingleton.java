package com.lux;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/10/22
 * @Time: 6:53 PM
 */
public class CycleSingleton
{
    // Attributes.
    private static CycleSingleton singleInstance = null;
    private int currentCycle;

    // Constructors.
    private CycleSingleton()
    {
        this.currentCycle = 0;
    }

    // Getters.
    public int getCurrentCycle()
    {
        return this.currentCycle;
    }

    // Setters.
    public void setCurrentCycle(int newCurrentCycle)
    {
        this.currentCycle = newCurrentCycle;
    }

    // Methods.
    public static CycleSingleton getInstance()
    {
        if (singleInstance == null)
        {
            singleInstance = new CycleSingleton();
        }
        return singleInstance;
    }

}
