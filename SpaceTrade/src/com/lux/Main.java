package com.lux;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SpaceException
    {
	    // Creating the commercial space
        CommercialSpace commercialSpace = new CommercialSpace();

        // Execution loop
        while ( commercialSpace.getCurrentCycleSingleton().getCurrentCycle() != commercialSpace.getMaxCycle() )
        {
            System.out.println("Current cycle : " + commercialSpace.getCurrentCycleSingleton().getCurrentCycle());
            int valueWantToSeeResponse = 1;
            while (valueWantToSeeResponse == 1)
            {
                Scanner wantToSeeResponse = new Scanner(System.in);
                // Asking user if he wants to see something
                System.out.println("Do you want to see something? [1/2]" +
                        "\n 1- Yes" +
                        "\n 2- No");
                // The input is an integer statement
                if ( wantToSeeResponse.hasNextInt() )
                {
                    valueWantToSeeResponse = wantToSeeResponse.nextInt();
                    // User want to see something statement
                    if (valueWantToSeeResponse == 1)
                    {
                        commercialSpace.viewThings();
                    }
                }
                // The input isn't an integer statement
                else
                {
                    System.out.println("Input error ! Please enter an integer the next time is asked.");
                }
            }

            // Do cycle
            commercialSpace.doCycle();
        }
    }
}
