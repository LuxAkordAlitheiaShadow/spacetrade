# SpaceTrade

## Introduction

This scholar project is a simulator of turn by turn space trade for highlight object-oriented programming's concepts.

It's a space trade where ships can buy and sell product to planets.

## Resources

The UML diagrams of this project is in Resources folder in .vpp format (Visual Paradigm) and also exported in data.zip.

## Running project

This project was developed with IntelliJ with the compiler OpenJDK17.

You just need to run the Main.java with an IDE or compile the project to an executable.

## Features

At the creation of the simulation, few planet are created with one product. Ships are randomly affected to an initial planet with a random itinerary.
Each turn, a ship can sell or buy a specific quantity of product, according to the dangerousness of this one, travel to another planet or waiting for embark to a port.
When a planet have no more space to his port area, 2 per port and 1 to 3 port per planet, the ship is destroyed.

Each turn, you can see the list of product, planet and ship of this simulation, and you can see a transaction resume of specific planet or ship.

## Screenshot

![Ask_to_view_things](./Resources/Screenshot/Ask_to_view.png)

Prompter for asking user if he wants to see things and what.

![What_to_view](./Resources/Screenshot/What_to_view.png)

The user can choose what list he wants to see.

![List_of_Planet](./Resources/Screenshot/List_Planet.png)

List of Planets in a simulation.

![New_Ship](./Resources/Screenshot/New_ship.png)

Sometimes, a new ship is added to the commercial space.

![Ship_transaction](./Resources/Screenshot/Ship_transaction.png)

Users can see the transaction summary of specific ship.

![Bad_input](./Resources/Screenshot/Bad_Input.png)

Have entered a bad input ? Don't worry, we handle that.

![Destroyed_Ship](./Resources/Screenshot/Destroyed_Ship.png)

When they come to end of life, the ships are destroyed.
